'use strict';

module.exports = {
    UINavigation: require('./component/Navigation'),
    UINavigationItem: require('./component/NavigationItem'),
    UINVBox: require('./component/NVBox')
};