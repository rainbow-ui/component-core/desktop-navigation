import { Component } from "rainbowui-desktop-core";
import { Util } from "rainbow-desktop-tools";
import "../css/Navigation.css";
import PropTypes from 'prop-types';
import NavigationItem from "./NavigationItem";
import PubSub from 'pubsub-js';
export default class Navigation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            autoNavItemList: []
        }
        this.scrolling = false;
    }

    renderComponent() {
        return (
            <div id={this.componentId} className="sideNavigation">
                <nav className="cd-vertical-nav">
                    <ul>
                        {Util.parseBool(this.props.showNavigationBar) ? <li className="li-top" id="toolbar-tab-top"><a data-href={this.props.id + " toolbar-tab-top"}><span class="glyphicon glyphicon-chevron-up"></span></a></li> : ''}
                        {Util.parseBool(this.props.isAuto) ?
                            this.renderAutoNavItem() :
                            this.renderNavigationItem(this)}
                        {Util.parseBool(this.props.showNavigationBar) ? <li className="li-buttom" id="toolbar-tab-bottom"><a data-href={this.props.id + " toolbar-tab-bottom"}><span class="glyphicon glyphicon-chevron-down"></span></a></li> : ''}
                    </ul>
                </nav>
            </div>
        );
    }
    getNavItemList() {
        let list = [];
        let counter = 0;
        let navItemList = document.querySelectorAll("a[anchor=true]");
        navItemList.forEach(item => {
            counter++;
            let dictItem = {};
            if (item.getAttribute('id')) {
                dictItem['anchor'] = item.getAttribute('id');
            }
            if (item.getAttribute('anchorName')) {
                dictItem['anchorName'] = item.getAttribute('anchorName');
            }
            list.push(dictItem);
            if (counter === navItemList.length) {
                this.setState({ autoNavItemList: list });
            }
        });
    }

    // 自动渲染item
    renderAutoNavItem() {
        let { autoNavItemList } = this.state || [];
        return (
            autoNavItemList.map(item => {
                return (
                    <NavigationItem noI18n={Util.parseBool(this.props.noI18n)} label={item.anchorName} sectionId={item.anchor} />
                )
            })
        )
    }

    // 手动
    renderNavigationItem(component) {
        if (component.props.children != null) {
            return component.props.children.map(function (children) {
                return children;
            });
        }
    }
    componentDidMount() {
        // 自动渲染导航时接收参数
        if (Util.parseBool(this.props.isAuto)) {
            PubSub.publish('actionNavBox', null);
            PubSub.subscribe('startNavBox', () => {
                this.getNavItemList();
            });
        }

        const self = this;
        let nav = $("#" + this.componentId);
        this.contentSections = $('.card');
        this.verticalNavigation = $('.cd-vertical-nav');
        this.navigationItems = this.verticalNavigation.find('a');
        this.navTrigger = $('.cd-nav-trigger');
        this.scrollArrow = $('.cd-scroll-down');

        // $(window).on('scroll', this.checkScroll.bind(this,nav));

        //smooth scroll to the selected section
        this.verticalNavigation.on('click', 'a', function (event) {
            event.preventDefault();
            // self.smoothScroll($(this.hash));
            const id = $(this).attr("data-href");
            let outBox = self.props.desktopRightPage;
            if (id == self.props.id + " toolbar-tab-top") {
                $('#' + outBox).animate({ scrollTop: 0 }, 400);
            }
            if (id == self.props.id + " toolbar-tab-bottom") {
                // $('#' + outBox).animate({ scrollTop: $(document).height() }, 400);
                if ($('#' + outBox).length > 0) {
                    $('#' + outBox).animate({ scrollTop: $('#' + outBox)[0].scrollHeight }, 400);
                }
            }
            let anchorElement = document.getElementById(id);
            if (anchorElement) {
                anchorElement.scrollIntoView();
                window.scrollTo(0, $(window).scrollTop() - 140);
            }
            self.verticalNavigation.removeClass('open');
        });

        //smooth scroll to the second section
        this.scrollArrow.on('click', function (event) {
            event.preventDefault();
            self.smoothScroll($(this.hash));
        });

        // open navigation if user clicks the .cd-nav-trigger - small devices only
        this.navTrigger.on('click', function (event) {
            event.preventDefault();
            self.verticalNavigation.toggleClass('open');
        });
    }

    checkScroll(nav) {
        let _self = this;
        if (!nav.hasClass("navigation-toggle")) {
            nav.addClass("navigation-toggle");
            setTimeout(_self.removeNavigationClass.bind(_self, nav), 1200);
        }

        if (!this.scrolling) {
            this.scrolling = true;
            (!window.requestAnimationFrame) ? setTimeout(this.updateSections.bind(this), 300) : window.requestAnimationFrame(this.updateSections.bind(this));
        }
    }

    removeNavigationClass(nav) {
        nav.removeClass("navigation-toggle");
    }

    updateSections() {
        const self = this;
        const halfWindowHeight = $(window).height() / 2,
            scrollTop = $(window).scrollTop();
        this.contentSections.each(function () {
            const section = $(this),
                sectionId = section.attr('id'),
                navigationItem = self.navigationItems.filter('[data-href^="' + sectionId + '"]');
            ((section.offset().top - halfWindowHeight < scrollTop) && (section.offset().top + section.height() - halfWindowHeight > scrollTop))
                ? navigationItem.addClass('active')
                : navigationItem.removeClass('active');
        });
        this.scrolling = false;
    }

    smoothScroll(target) {
        $('body,html').animate(
            { 'scrollTop': target.offset().top },
            300
        );
    }

};


Navigation.propTypes = $.extend({}, Component.propTypes, {
    showNavigationBar: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    desktopRightPage: PropTypes.string
});


Navigation.defaultProps = $.extend({}, Component.defaultProps, {
    desktopRightPage: 'desktop_right_page'
});

