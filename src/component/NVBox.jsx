import React from 'react';
import PubSub from 'pubsub-js';
export default class NVBox extends React.Component {

    render() {
        return this.props.children || null;
    }

    componentWillReceiveProps(nextProps) {
        this.props = nextProps;
        PubSub.publish('startNavBox', null);
    }

    componentDidMount() {
        PubSub.subscribe('actionNavBox', () => {
            PubSub.publish('startNavBox', null);
        });

    }
}